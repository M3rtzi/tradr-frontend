import Vue from 'vue';
import './custom.scss'
import axios from 'axios';
import VueMq from "vue-mq"
import Vuelidate from 'vuelidate'
import BootstrapVue from 'bootstrap-vue';
import Snackbar from "./plugins/snackbar.js";
import App from '@/App.vue';
import router from '@/router.js';
import store from '@/store/index.js';


Vue.use(Snackbar);


axios.defaults.baseURL = "http://localhost:5000" // "https://api.tradr.se"


Vue.use(VueMq, {
  breakpoints: { // copied from bootstrap _variables.scss grid-breakpoints
    xs: 0,
    sm: 576,
    md: 768,
    lg: 992,
    xl: 1200,
  },
  defaultBreakpoint: "sm"
});
Vue.use(Vuelidate);
Vue.use(BootstrapVue);


Vue.filter("capitalize", function (value) {
  if (!value) return ""
  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
})


new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
