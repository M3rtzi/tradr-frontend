import Vue from "vue";
import Router from "vue-router";
/*Views*/
import Auth from "@/views/Auth.vue";
import TradrSheet from "@/views/TradrSheet.vue";
import WatchlistSummary from "@/views/WatchlistSummary.vue";
import WatchlistDetails from "@/views/WatchlistDetails.vue";
import Simulation from "@/views/Simulation.vue";
import Strategy from "@/views/Strategy.vue";
import Introduction from  "@/views/Introduction.vue"
import Error404 from "@/views/Error404.vue";


Vue.use(Router);


const routes = [
  {
    name: "auth",
    path: "/",
    component: Auth,
    meta: { requiresAuth: false },
  },
  {
    name: "watchlist",
    path: "/watchlist",
    component: TradrSheet,
    props: { links: [{ name: "summary" }, { name: "details" }]},
    redirect: { name: "summary" },
    meta: { requireAuth: true },
    children: [
      {
        name: "summary",
        path: "summary",
        component: WatchlistSummary,
        meta: { requiresAuth: true },
      },
      {
        name: "details",
        path: "details",
        component: WatchlistDetails,
        meta: { requiresAuth: true },
      },
    ],
  },
  {
    name: "lab",
    path: "/lab",
    component: TradrSheet,
    props: { links: [{ name: "strategy" }, { name: "simulation" }]},
    redirect: { name: "strategy" },
    meta: { requiresAuth: true },
    children: [
      {
        name: "strategy",
        path: "strategy/:id?",
        props: true,
        component: Strategy,
        meta: { requiresAuth: true },
      },
      {
        name: "simulation",
        path: "simulation/:id?",
        props: true,
        component: Simulation,
        meta: { requiresAuth: true },
      },
    ],
  },
  {
    name: "docs",
    path: "/docs",
    component: TradrSheet,
    children: [
      {
        name: "intro",
        path: "",
        component: Introduction
      },
    ],
  },
  {
    path: "*",
    component: Error404,
  },
];


const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: routes,
});


router.beforeEach((to, from, next) => {
  let requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  let user = localStorage.user ? JSON.parse(localStorage.user) : false
  let validToken = user && Date.parse(user.token_expiration) > Date.now()
  if (requiresAuth && !validToken) {
    next('/');
  }
  else if (to.path === '/' && validToken) {
    next('/watchlist');
  }
  else {
    next();
  }
})


export default router;
