export default {
  install(Vue, options) {
    Vue.prototype.$setAlert = function(alert) {
      this.$store.dispatch("snackbar/setAlert", alert);
    };
    Vue.prototype.$deleteAlert = function(alert) {
      this.$store.dispatch("snackbar/deleteAlert", alert);
    };
  },
};
