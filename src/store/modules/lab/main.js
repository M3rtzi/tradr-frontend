import versions from "./versions.js";
import charts from "./charts.js";
import makeStore from "./makeStore.js";
import helpers from "./helpers.js";
import axios from "axios";

//TODO: Remove everything that has to do with "active type" etc.
const state = {
  //activeType: "watch",
  //types: ["watch", "script", "simulation"]
}


const getters = {
  getActiveType: (state) => state.activeType,
  getTypes: (state) => state.types,
}


const mutations ={
  setActiveType: (state, type) => {
    state.activeType = type;
  },
}


const actions = {
  setActiveType: ({ commit }, type) => {
    commit("setActiveType", type);
  },
}


export default {
  namespaced: true,
  modules: {
    watch: makeStore("watch", { watchSims:
      makeStore("watchSims", { charts })
    }),
    script: makeStore("script", { versions }),
    simulation: makeStore("simulation", { charts }), // extend for 202 response or use for all 202?
    symbol: makeStore("symbol"),
  },
  state,
  mutations,
  getters,
  actions,
}
