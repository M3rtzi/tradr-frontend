import helpers from "./helpers.js";
import axios from "axios";
import Vue from "vue";


export default function (name, modules) {
  return {
    namespaced:true,
    modules: modules,
    state: {
      ids: [],
      items: {},
      activeId: null,
      _meta: {},
      _links: {},
    },
    getters: {
      getItem: (state) => (id) => state.items[id],
      getActiveItem: (state) => state.items[state.activeId],
      getFiltered: (state, getters) => (id, keys) => {
        return Object.keys(state.items[id])
        .filter(key => !keys.includes(key))
        .reduce((obj, key) => {
          return { ...obj, [key]: state.items[id][key] };
        }, {});
      },
      getItemsWithAttr: (state) => (key, value) => {
        return Object.values(state.items).filter(item => item[key] === value);
      },
      getItemsArray: (state) => state.ids.map((id) => state.items[id]),
      getFormSelect: (state) => (key) => {
        return Object.values(state.items).map((item) => {
          return { text: item[key], value: item.id }
        });
      },
      nextPageExist: (state) => state._links.next,
    },
    mutations: {
      setActiveId: (state, id) => {
        state.activeId = id;
      },
      setItem: (state, item) => {
        if (!(item.id in state.items)) {
          state.ids.push(item.id);
        }
        Vue.set(state.items, item.id, item);
      },
      setMeta: (state, meta) => {
        Vue.set(state, "_meta", meta)
      },
      setLinks: (state, links) => {
        Vue.set(state, "_links", links)
      },
      deleteItem: (state, { id }) => {
        Vue.delete(state.items, id);
        state.ids.splice(state.ids.indexOf(id), 1);
      },
      clearState: (state) => {
        state.ids = [];
        state.items = {};
        state.activeId = null;
        state._meta = {};
        state._links = {};
      },
    },
    actions: {
      setActiveId: async ({ commit }, id) => {
        commit("setActiveId", id);
      },
      createItem: async ({ dispatch, commit, rootState }, item) => {
        return new Promise(function(resolve, reject) {
          axios.post(
            helpers.http.url(`/api/${name}`), // configured from store name
            item,
            helpers.http.headers(rootState.user.token)
          ).then(response => {
            if (response.status == 202 && response.data._links.location) {
              commit("setItem", {
                ...response.data,
              });
              const interval = setInterval(() => {
                axios.get(
                  helpers.http.url(response.data._links.location),
                  helpers.http.headers(rootState.user.token)
                ).then((taskstatus) => {
                  /*SUCCESS - clearInterval & set status*/
                  if (["SUCCESS", "FAILURE"].includes(taskstatus.data.state)) {
                    clearInterval(interval)
                    dispatch("readItem", response.data._links.self)
                  }
                  /*not SUCCESS - set status*/
                  commit("setItem", {
                    ...response.data,
                    ...{ status: taskstatus.data.state }
                  });
                }, err => {
                  console.error(err);
                });
              }, 1000)

            }
            else {
              commit("setItem", response.data);
            }
            resolve(response);
          }, error => {
            reject(error);
          });
        });
      },
      updateItem: async ({ commit, rootState }, item) => {
        const response = await axios.put(
          helpers.http.url(item._links.self),
          item,
          helpers.http.headers(rootState.user.token)
        );
        commit("setItem", { ...item, ...response.data }); // should client data & server data be merged on the component or in the store?
      },
      readItem: ({ commit, rootState}, url) => {
        helpers.actions.getItem("setItem") (
          commit,
          rootState,
          url
        )
      },
      readItems: async ({ state, commit, rootState }, { url }) => {
        helpers.actions.getItems2("setItem")(
          commit,
          rootState,
          url,
        );
      },
      /*deleteItem: async ({ commit, rootState }, item) => {
        const response = await axios.delete(
          helpers.http.url(item._links.self),
          helpers.http.headers(rootState.user.token),
        );
        commit("deleteItem", item);
      },*/
      deleteItem: ({ commit, rootState }, item) => {
        return new Promise(function(resolve, reject) {
          axios.delete(
            helpers.http.url(item._links.self),
            helpers.http.headers(rootState.user.token),
          ).then(response => {
            commit("deleteItem", item);
            resolve(response)
          }, err => {
            reject(err)
          });
        });
      },
      nextPage: async ({ commit, rootState, state }) => {
        if (state._links.next) {
          helpers.actions.getItems(
            commit,
            rootState,
            state._links.next,
          );
        }
      },
    }
  }
}
