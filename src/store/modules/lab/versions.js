import helpers from "./helpers.js";
import Vue from "vue";


const state = {
  ids: [],
  versions: {},
  _meta: {},
  _links: {},
}


const getters = {
  getVersion: (state) => (id) => (transaction_id) => state.versions[id][transaction_id],
  getVersions: (state) => (id) => state.versions[id],
  getFormSelect: (state) => (id, key) => {
    const versions = state.versions[id];
    if (versions) {
      return Object.values(versions).map((version) => {
        return {
          text: version[key] && key == "updated" ? version[key] : "Initial version",
          value: { id: version.id, transaction_id: version.transaction_id }
        }
      })
    }
  },
}


const mutations = {
  setVersion: (state, version) => {
    if (!(version.id in state.versions)) {
      Vue.set(state.versions, version.id, {});
      state.ids.push(version.id);
    }
    Vue.set(state.versions[version.id], version.transaction_id, version);
  },
  setMeta: (state, meta) => {
    Vue.set(state, "_meta", meta)
  },
  setLinks: (state, links) => {
    Vue.set(state, "_links", links)
  },
}


const actions = {
  readVersions: async ({ commit, rootState }, { url }) => {
    helpers.actions.getItems2("setVersion") (
      commit,
      rootState,
      url,
    );
  },
  nextPage: async ({ commit, rootState, state }) => {
    if (state._links.next != null) {
      helpers.actions.getItems2("setVersion") (
        commit,
        rootState,
        self._links.next,
      );
    }
  }
}


export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
