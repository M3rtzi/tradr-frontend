import helpers from "./helpers.js";
import Vue from "vue";
import axios from "axios";
import styles from "@/custom.scss"


const state = {
  ids: [],
  labels: {},
  options: {},
  datasets: {},
  urlCache: {},
  stdConf: {
    script: {
      //yAxisID: "y-axis-1",
      borderColor: styles.secondary,
      borderWidth: 1,
      fill: false,
    },
    index: {
      //yAxisID: "y-axis-0",
      borderColor: styles.primary10,
      borderWidth: 1,
      backgroundColor: styles.primary10,
      fill: false,
    }
  },
}


const getters = {
  getLabels: (state) => (id) => state.labels[id],
  getDatasetByLabel: (state) => (id) => (label) => {
    return state.datasets[id].filter((dataset) => dataset.label === label);
  },
  getDatasets: (state) => (id) => state.datasets[id],
  getDatasetsExtremes: (state) => (id) => {
    let combine = (extremes) => { return {
      min: Math.min(...extremes.map((extreme) => { return extreme.min })),
      max: Math.max(...extremes.map((extreme) => { return extreme.max })),
    }};
    return combine(state.datasets[id].map(dataset => {
      return {
        min: Math.min(...dataset.data) - dataset.data[0],
        max: Math.max(...dataset.data) - dataset.data[0],
    }}));
  },
  getDatasetBoundsByYAxisID: (state, getters) => (id, yAxisID) => {
    const extremes = getters.getDatasetsExtremes(id);
    const dataset = state.datasets[id].filter(
      (dataset) => dataset.yAxisID === yAxisID
    )[0];
    if (dataset) {
      return {
        min: dataset.data[0] + extremes.min,
        max: dataset.data[0] + extremes.max
      }
    }
  },
  getOptions: (state) => (id) => state.options[id],
}


const mutations = {
  setDataset: (state, { dataset, id }) => {
    if (!(id in state.datasets)) {
      Vue.set(state.datasets, id, []);
      state.ids.push(id);
    }
    state.datasets[id].push(dataset);
  },
  setLabels: (state, { labels, id }) => {
    Vue.set(state.labels, id, labels);
  },
  setOptions: (state, { options, id }) => {
    Vue.set(state.options, id, options);
  },
  setUrlCache: (state, { url, ref }) => {
    Vue.set(state.urlCache, url, ref);
  },
  clearState: (state) => {
    state.ids = [];
    state.labels = {};
    state.options = {};
    state.datasets = {};
    state.urlCache = {};
  },
}


const actions = {
  readDataset: async ({ state, getters, commit, rootState }, { url, id, kwargs}) => {
    var dataset = null
    if (url in state.urlCache) {
      const cached = state.urlCache[url];
      dataset = getters.getDatasetByLabel(cached.id)(cached.label)[0];
    }
    else {
      const response = await axios.get(
        helpers.http.url(url),
        helpers.http.headers(rootState.user.token)
      );
      dataset = response.data;
    }
    if (!(id in state.datasets && getters.getDatasetByLabel(id)(dataset.label)[0])) {
      commit("setDataset", { dataset: {...dataset, ...kwargs, id: id }, id: id });
      commit("setUrlCache", { url: url, ref: { id: id, label: dataset.label }});
    }
  },
  readDataset2: ({ state, getters, commit, rootState }, { url, id }) => {
    return new Promise(function(resolve, reject) {
      axios.get(
        helpers.http.url(url),
        helpers.http.headers(rootState.user.token))
        .then(response => {
          if (!(id in state.datasets && getters.getDatasetByLabel(id)(response.data.label)[0])) {
            commit("setDataset", { dataset: {...response.data, id: id }, id: id });
            commit("setUrlCache", { url: url, ref: { id: id, label: response.data.label }});
          };
          resolve(response.data);
        }, error => {
          reject(error)
      });
    });
  },
  readLabels: async ({ commit, rootState }, { url, id }) => {
    var labels = null
    if (url in state.urlCache) {
      const cached = state.urlCache[url];
      labels = getters.getLabels(cached.id);
    }
    else {
      const response = await axios.get(
        helpers.http.url(url),
        helpers.http.headers(rootState.user.token)
      );
      labels = response.data;
    }
    if (!(id in state.labels)) {
      commit("setLabels", { labels: labels, id: id });
      commit("setUrlCache", { url: url, ref: { id: id }});
    }
  },
  readLabels2: ({ commit, rootState }, { url, id }) => {
    return new Promise(function(resolve, reject) {
      axios.get(
        helpers.http.url(url),
        helpers.http.headers(rootState.user.token)
      ).then(response => {
        if (!(id in state.labels)) {
          commit("setLabels", { labels: response.data, id: id });
          commit("setUrlCache", { url: url, ref: { id: id }});
        };
        resolve(response.data);
      }, error => {
        reject(error);
      });
    });
  },
  readDefault2: ({ state, dispatch }, { status, id, _links, }) => {
    if (status !== "SUCCESS") {
      return
    }
    const labels = dispatch("readLabels2", { id: id, url: _links.labels });
    const script = dispatch("readDataset2", { id: id, url: _links.return, kwargs: state.stdConf.script });
    return new Promise(function(resolve, reject) {
      Promise.all([
        dispatch("readLabels2", { id: id, url: _links.labels }),
        dispatch("readDataset2", { id: id, url: _links.return }),
        dispatch("readDataset2", { id: id, url: _links.index }),
        dispatch("readDataset2", { id: id, url: _links.signal }),
      ]).then((values) => {
        resolve({ ...values[0], datasets: [ values[1], values[2] ], _meta: { signals: values[3] } });
      }, error => {
        reject(error);
      });
    });
  },
  readDefault: async ({ state, dispatch }, { status, id, _links }) => {
    if (status !== "SUCCESS") {
      return
    }
    await dispatch("readLabels2", { id: id, url: _links.labels });
    await dispatch("readDataset", { id: id, url: _links.return });
    await dispatch("readDataset", { id: id, url: _links.index });
  }
}



export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
}
