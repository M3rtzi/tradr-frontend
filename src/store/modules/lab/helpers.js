import axios from "axios";
import Vue from "vue";


const  mutations = {
  items: (key) => (state, item, id) => {
    Vue.set(state[key], id, item);
  },
  pagination: (key) => (state, item) => {
    Vue.set(state, key, item)
  },
}


const actions =  {
  getItem: (mutation) => async (commit, rootState, url) => { // key, id
    const response = await axios.get(
      http.url(url),
      http.headers(rootState.user.token)
    );
    commit(mutation, response.data); // { [key]: response.data, id: id }
  },
  getItems: async (commit, rootState, url) => {
    /*depricated*/
    const response = await axios.get(
      http.url(url),
      http.headers(rootState.user.token)
    );
    commit("setMeta", response.data._meta);
    commit("setLinks", response.data._links);
    response.data.items.forEach((item) => {
      commit("setItem", item);
    });
  },
  getItems2: (mutation) => async (commit, rootState, url) => {
    const response = await axios.get(
      http.url(url),
      http.headers(rootState.user.token)
    );
    commit("setMeta", response.data._meta);
    commit("setLinks", response.data._links);
    await response.data.items.forEach((item) => {
      commit(mutation, item);
    });
  },
}


const http = {
  url: ((url) => `${axios.defaults.baseURL}${url}`),
  headers: ((jwt) => ({ headers: { Authorization: `Bearer ${jwt}` } })),
  urlencode: ((params) => {
    const esc = encodeURIComponent;
    return Object.keys(params).map(
      k => esc(k) + '%3D' + esc(params[k])
    ).join('%26');
  }),
}


const chart = {
  alignTicks: () => {},
}


export default {
  mutations,
  actions,
  http,
  chart,
};
