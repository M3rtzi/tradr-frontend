import Vue from "vue";


const state = {
  id: [],
  message: {},
  variant: {},
  duration: {},
};


const getters = {
  id: (state) => state.id[state.id.length - 1],
  alert: (state, getters) => {
    const id = getters.id
    return typeof(id) === "number" ? {
      id: id,
      message: state.message[id],
      variant: state.variant[id],
      duration: state.duration[id]
    } : null
  },
  alerts: (state) => {
    return state.id.map(id => {
      return {
        id: id,
        message: state.message[id],
        variant: state.variant[id],
        duration: state.duration[id],
      };
    })
  },
};


const mutations = {
  setAlert: (state, alert) => {
    for (var key in alert) {
      key === "id"
      ? state.id.push(alert["id"])
      : Vue.set(state[key], alert["id"], alert[key]);
    };
  },
  deleteAlert: (state, alert) => {
    for (var key in alert) {
      key === "id"
      ? state.id.splice(state.id.indexOf(alert["id"]), 1)
      : Vue.delete(state[key], alert["id"])
    };
  },
};


const actions = {
  setAlert: ({ state, commit }, alert) => {
    const id = state.id[state.id.length - 1]
    commit("setAlert", {
      id: id >= 0 ? id + 1 : 0,
      variant: "primary",
      duration: 4,
      ...alert
    })
  },
  deleteAlert: ({ state, commit }, alert) => {
    commit("deleteAlert", alert)
  },
};


export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
