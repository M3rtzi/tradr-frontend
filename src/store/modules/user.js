/*eslint-disable*/
import helpers from "./lab/helpers.js";
import router from "@/router";
import axios from "axios";
import Vue from "vue";


const http = {
  url: ((url) => `${axios.defaults.baseURL}${url}`),
  headers: ((jwt) => ({ headers: { Authorization: `Bearer ${jwt}` } })),
  basicAuthHeaders: (email, password) => {
    return { headers: { Authorization: "Basic " + btoa(`${email}:${password}`) } }
  },
}


const state = {}


const mutations = {
  setUser: (state, userData) => {
    for (var key in userData) {
      Vue.set(state, key, userData[key])
    }
  },
  loginUser: (state, userData) => {
    for (var key in userData) {
      Vue.set(state, key, userData[key])
    }
    localStorage.setItem("user", JSON.stringify(userData));
  },
  logoutUser: (state) => {
    for (var key in state) {
      Vue.set(state, key, null);
    }
    localStorage.setItem("user", null);
  },
}


const getters = {
  isAuthenticated: (state) => {
    return state.token !== null && Date.parse(state.token_expiration) > Date.now()
  },
  isCached: (state) => {
    if (typeof(localStorage.user) == "undefined") {
      return false;
    }
    let user = JSON.parse(localStorage.user);
    return user !== null && Date.parse(user.token_expiration) > Date.now() && user.token !== null;
  },
};


function filterSimulations(item) {
  return item._links.watch === null
}


const actions = {
  create: ({}, userData) => {
    return axios.post(
      http.url("/api/user"),
      userData
    )
  },
  loadUser: async ({ state, dispatch, rootState }) => {
    dispatch(
      "lab/symbol/readItems",
      { url: state._links.symbols },
      { root: true }
    );
    dispatch(
      "lab/watch/readItems",
      { url: state._links.watchlist },
      { root: true }
    );
    dispatch(
      "lab/watch/watchSims/readItems",
      { url: state._links.watchlistSimulations },
      { root: true }
    );
    dispatch(
      "lab/script/readItems",
      { url: state._links.scripts },
      { root: true }
    );
    dispatch(
      "lab/simulation/readItems",
      { url: state._links.simulations },
      { root: true }
    );
  },
  resetUser: ({}, data) => {
    console.log(data);
    return axios.put(
      http.url("/api/user/reset"),
      data
    );
  },
  updateUser: ({ commit, state }, item) => {
    return axios.put(
      http.url(state._links.self),
      item,
      http.headers(state.token)
    ).then((response) => {
      commit("setUser", response.data )
    });
  },
  login: ({ commit, state, dispatch }, userData) => {
    return axios.post(
      http.url("/api/tokens"),
      '',
      http.basicAuthHeaders(userData.email, userData.password)
    ).then((response) => {
      commit("loginUser", response.data);
      dispatch("loadUser");
      router.push("/watchlist");
    });
  },
  autoLogin: ({ commit, getters, dispatch }) => {
    if (!getters.isCached) {
      return;
    };
    commit("loginUser", JSON.parse(localStorage.getItem("user")));
    dispatch("loadUser");
  },
  logout: ({ commit }) => {
    commit("logoutUser");
    commit("lab/symbol/clearState", null, { root: true });
    commit("lab/watch/clearState", null, { root: true });
    commit("lab/script/clearState", null, { root: true });
    commit("lab/simulation/clearState", null, { root: true });
    router.push("/");
  },
};


export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
};
