import Vue from 'vue';
import Vuex from 'vuex';
import user from './modules/user';
import lab from "./modules/lab/main.js"
import snackbar from "./modules/snackbar.js"


Vue.use(Vuex);


export default new Vuex.Store({
  modules: {
    snackbar,
    user,
    lab,
  },
});
