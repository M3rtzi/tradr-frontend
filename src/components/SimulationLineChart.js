import { Line } from "vue-chartjs";
import styles from "@/custom.scss";


const TradrPlugin = {
  id: "tradr-plugin",
  beforeDraw: (chart, easing) => {
    if (chart.config.options.chartArea && chart.config.options.chartArea.backgroundColor) {
      var ctx = chart.chart.ctx;
      var chartArea = chart.chartArea
      ctx.save();
      ctx.fillStyle = chart.config.options.chartArea.backgroundColor;
      ctx.fillRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
      ctx.restore();
    };
  },
};


export default {
  name: "simulationLineChart",
  extends: Line,
  props: {
    data: {
      type: Object,
      default: null,
    },
  },
  watch: {
    //Custom watcher to re-render instead of update chart
    chartData() {
      this.$data._chart.destroy();
      this.renderChart(this.chartData, this.options);
    },
  },
  computed: {
    extremes() {
      const extremes = this.data.datasets.map(dataset => {
        return {
          min: Math.min( ...dataset.data) - dataset.data[0],
          max: Math.max( ...dataset.data) - dataset.data[0],
        };
      });
      return {
        min: Math.min( ...extremes.map(extreme => extreme.min) ),
        max: Math.max( ...extremes.map(extreme => extreme.max) ),
      };
    },
    delta() {
      return this.extremes.max - this.extremes.min;
    },
    options() {
      return {
        responsive: true,
        maintainAspectRatio: true,
        aspectRatio: this.$mq === "sm"
          ? 8 / 7
          : 5 / 2,
        legend: {
          display: false,
        },
        elements: {
          line: {
            tension: 0,
            borderWidth: 1,
          },
          point: {
            hitRadius: 8,
          },
        },
        chartArea: {
          backgroundColor: styles.primary,
        },
        scales: {
          xAxes: [
            {
              type: "time",
              distribution: "series",
              ticks: {
                z: 1,
                fontColor: styles.primary,
                padding: 12,
                maxTicksLimit: this.$mq === "sm"
                  ? 6
                  : 8,
                maxRotation: 0,
                callback: (value, index, values) => {
                  return (index === 0 || index === values.length - 1)
                    ? null
                    : value;
                },
              },
              gridLines: {
                display: true,
                drawTicks: false,
                drawOnChartArea: true,
                color: styles.primaryD3,
              },
              scaleLabel: {
                display: false,
              },
              afterFit: (axis) => {
                axis.paddingLeft = 0;
                axis.paddingRight = 0;
              },
            }
          ],
          yAxes: [
            {
              position: "right",
              ticks: {
                z: 1,
                fontColor: styles.primary30,
                fontStyle: "bold",
                mirror: true,
                padding: -12,
                maxTicksLimit: this.$mq === "sm"
                  ? 6
                  : 8,
                callback: (value, index, values) => {
                  return (index === 0 || index === values.length - 1)
                    ? null
                    : value + " $";
                },
              },
              gridLines: {
                display: true,
                drawTicks: false,
                color: "transparent",
                zeroLineColor: styles.primaryD3,
              },
              scaleLabel: {
                display: false,
              },
              beforeSetDimensions: (axis) => {
                return this.ticks(this.data.datasets[0], axis)
              },
              afterFit: (axis) => {
                axis.paddingBottom = 0;
                axis.paddingTop = 0;
              },
            },
            ...this.data.datasets.map((dataset, index) => {
              if (index != 0) {
                return {
                  ticks: {
                    display: false,
                  },
                  gridLines: {
                    display: false,
                    drawTicks: false,
                  },
                  scaleLabel: {
                    display: false,
                  },
                  beforeSetDimensions: (axis) => {
                    return this.ticks(dataset, axis)
                  },
                  afterFit: (axis) => {
                    axis.paddingBottom = 0;
                    axis.paddingTop = 0;
                  },
                };
              } else {
                return null;
              };
            }).filter(yAxis => {
              if (yAxis) {
                return yAxis;
              };
            }),
          ],
        },
        tooltips: {
          mode: "index",
          xPadding: 12,
          yPadding: 12,
          caretSize: 8,
          backgroundColor: styles.primaryD3,
          titleFontSize: 14,
          titleFontColor: styles.primary50,
          titleMarginBottom: 8,
          bodySpacing: 8,
          footerFontStyle: "normal",
          footerFontColor: styles.primary50,
          footerMarginTop: 8,
          multiKeyBackground: null,
          callbacks: {
            title: (tooltipItem, data) => {
              const signal = data._meta.signals.data[tooltipItem[0].index];
              return this.$options.filters.capitalize(signal
                ? signal
                : "hold"
              );
            },
            label: (tooltipItem, data) => {
              return " "
                +  data.datasets[tooltipItem.datasetIndex].label
                + ": "
                + parseFloat(tooltipItem.yLabel).toFixed(1)
                + "$";
            },
            labelTextColor: (tooltipItem, chart) => {
              return tooltipItem.datasetIndex === 0
                ? styles.secondary20
                : styles.primary30;
              },
            footer: (tooltipItem, data) => {
              return tooltipItem[0].label.match(/[0-9]+\s[A-Za-z]+\s\d+/);
            },
          },
        },
      };
    },
    chartData() {
      return {
        ...this.data,
        ...{ datasets: this.data.datasets.map((dataset, index) => {
          return {
            ...dataset,
            ...{
              yAxisID: `y-axis-${index}`,
              backgroundColor: index === 1
                ? "rgba(35, 85, 70, .4)"
                : null,
              fill: index === 1
                ? "bottom"
                : false,
              borderColor: index === 0
                ? styles.secondary20
                : styles.primary10,
              pointRadius:
                index === 0
                ? dataset.data.map((data, i) => {
                  return ["buy", "sell"].includes(this.data._meta.signals.data[i])
                    ? 5
                    : 0;
                })
                : 0,
              pointBackgroundColor: index === 0
                ? styles.secondary20
                : styles.primary10,
              pointStyle: dataset.data.map((data, index) => {
                switch (this.data._meta.signals.data[index]) {
                  case "buy":
                    return "crossRot";
                  case "sell":
                    return "rect";
                  default:
                    return "circle";
                };
              }),
              pointBorderColor: index === 0
                ? styles.secondary30
                : styles.primary20,
            },
          };
        })},
      };
    },
  },
  methods: {
    ticks(dataset, axis, paddingFactor = .1) {
      try {
        const padding = this.delta * paddingFactor;
        axis.options.ticks = {
          ...axis.options.ticks,
          min: dataset.data[0] + this.extremes.min - padding,
          max: dataset.data[0] + this.extremes.max + padding,
        };
      } catch(err) {
        console.error(err);
      }
    },
  },
  mounted () {
    this.addPlugin(TradrPlugin)
    this.renderChart(this.chartData, this.options)
  },
}
