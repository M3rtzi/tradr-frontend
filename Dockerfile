# build stage
FROM node:lts-alpine as build-stage
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY ./ .
RUN npm run build

#production stage
FROM nginx:stable-alpine as production-stage
MAINTAINER Jakob Lilliemarck <lilliemarckjakob@gmail.com>
RUN mkdir /app
COPY --from=build-stage /app/dist /app
COPY nginx.conf /etc/nginx/nginx.conf
